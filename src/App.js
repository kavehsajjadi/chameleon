import React from 'react'
import {parseCSS} from './cssToJss'
import { Grid, Row, Col, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';


class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {jss: ''}
	}

	render() {
		return (
			<Grid>
				<Row>
					<Col sm={6}>
				    <FormGroup controlId="formControlsTextarea">
				      <ControlLabel>CSS</ControlLabel>
				      <FormControl componentClass="textarea" placeholder="textarea" 
				      	onChange={e => this.setState({jss: parseCSS(e.target.value)})} />
				    </FormGroup>
					</Col>
					<Col sm={6}>
					<pre>
					{this.state.jss}
					</pre>
				    {/*<FormGroup controlId="formControlsTextarea">
				      <ControlLabel>JSS</ControlLabel>
				      <FormControl value={this.state.jss} componentClass="textarea" placeholder="textarea" />
				    </FormGroup>*/}
					</Col>
				</Row>
			</Grid>
		)
	}
}

export {App}