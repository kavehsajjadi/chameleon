'use strict'
/**
 * Shamelessly ripped from https://github.com/joshgillies/jss-cli
 **/
// body { font-size: 12px; }
import css from 'css'
import {pretty} from 'js-object-pretty-print'
import util from 'util'

function parseCSS(code, options) {
    try {
        var ast = css.parse(code)

        if (ast.stylesheet && ast.stylesheet.rules) {
            var jss = toJssRules(ast.stylesheet.rules)
            var output = pretty(jss)
            return output
        }

        return ''
    } catch(e) {
        return ''
    }

}
function stringify(obj_from_json){
    if(typeof obj_from_json !== "object" || Array.isArray(obj_from_json)){
        // not an object, stringify using native function
        return JSON.stringify(obj_from_json);
    }
    // Implements recursive object serialization according to JSON spec
    // but without quotes around the keys.
    let props = Object
        .keys(obj_from_json)
        .map(key => `${key}:${stringify(obj_from_json[key])}`)
        .join(",");
    return `{${props}}`;
}
function camelify(string) {
    const noDashes = string.replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); });
    const capitalWebkit = noDashes.replace('webkit', 'Webkit')
    const capitalMoz = capitalWebkit.replace('moz', 'Moz')
    return capitalMoz
}

function toJssRules(cssRules) {
    var jssRules = {}

    function addRule(rule, rules) {
        var key, style = {}
        key = rule.selectors.map(selector => camelify(selector)).join(', ')
        rule.declarations.forEach(function (decl) {
            style[camelify(decl.property)] = decl.value
        })
        rules[key] = style
    }

    cssRules.forEach(function (rule) {
        switch (rule.type) {
            case 'rule':
                addRule(rule, jssRules)
                break
            case 'media':
                var key = '@media ' + rule.media
                var value = {}
                rule.rules.forEach(function(rule) {
                    addRule(rule, value)
                })
                jssRules[key] = value
                break
            case 'font-face':
                var key = '@' + rule.type
                var value = {}
                rule.declarations.forEach(function (decl) {
                    value[decl.property] = decl.value
                })
                jssRules[key] = value
                break
            case 'keyframes':
                var key = '@' + rule.type + ' ' + rule.name
                var value = {}
                rule.keyframes.forEach(function (keyframe) {
                    var frameKey = keyframe.values.join(', ')
                    var frameValue = {}
                    keyframe.declarations.forEach(function (decl) {
                        frameValue[decl.property] = decl.value
                    })
                    value[frameKey] = frameValue
                })
                jssRules[key] = value
        }
    })

    return jssRules;
}

export {parseCSS}